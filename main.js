let arr = ["Kharkiv", "Kyiv", ["Boryspil", "Irpin"], ["Kharkiv", "Kyiv", ["Boryspil", "Irpin"], "Odesa", "Lviv", "Dnipro"], "Odesa", "Lviv", "Dnipro"]

function showListFromArray(arr, parent = document.body) {
    let ul = document.createElement('ul');

    for (let el of arr) {
        let list = document.createElement('li');

        if (Array.isArray(el)) {
            showListFromArray(el, list)
        }
        else {
            list.textContent = el;
        }
        ul.append(list);
    }

    // ------------or using map----------------
    // arr.map((el) => {
    //     let list = document.createElement('li');
    //     if (Array.isArray(el)) {
    //         showListFromArray(el, list)
    //     }
    //     else {
    //         list.textContent = el;
    //     }
    //     ul.append(list);
    // })
    // -------------------------------------------------------------------

    parent.append(ul)
}
showListFromArray(arr);


let timerHeading = document.createElement('p');
timerHeading.textContent = 'The page will be reloaded after...'
document.body.prepend(timerHeading);
let timerSpan = document.createElement('h2');
timerHeading.append(timerSpan)
timerHeading.classList.add('heading-timer')
timerSpan.classList.add('heading-timer-p')
let counter = 4;

let timer = setInterval(function () {
    counter--;
    timerSpan.textContent = `${counter} seconds`;
    if (counter === 0) {
        clearInterval(timer);
        document.body.remove()
    }
}, 1000)